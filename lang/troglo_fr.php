<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'legend' => 'Formulaire d\'inscription pour la participations aux 10 ans de SPIP',
'label_pseudo' => 'Prénom/Nom ou Pseudo',
'label_email' => 'Email',
'label_nbadultes' => 'Nombre d\'adultes',
'label_nbenfants' => 'Nombre d\'enfants',
'label_jour' => 'Cochez les jours où vous serez présent',
'label_jour_vendredi' => 'Vendredi',
'label_jour_samedi' => 'Samedi',
'label_jour_dimanche' => 'Dimanche',

);

?>